import controlP5.*;
import processing.video.*;


/*
*  \name:  filtros
*  \brief:ejemplo de diferentes filtros para el curso de introduccion en Semanai
*  \date: 26/09/17
*  \autor: Oscar Hinojosa
*/

/*Declaracion de variables globales de este skech*/
ControlP5 cp5;
Button B1,B2;
Slider s1;
Textlabel l1;
PImage img,img2,img3;
int state=0;
int limite=100;
Capture c;
void setup()
{
 size(1080,920); 
 /***************************GUI*****************************************/
  cp5 = new ControlP5(this);/* creacion de constructor*/
  img = new PImage (320, 240, RGB);
  s1= cp5.addSlider("Porcentaje")
   .setPosition(500,890)
   .setSize(150,20)/*tamaño del slider*//*Ubicacion del slider*/
   .setRange(0,100)/*Asignas el rango de movimiento*/
   .setValue(50)/*valor de inicio*/
   .setNumberOfTickMarks(101);   /*fatla una configuracion*/
  B1 =cp5.addButton("Next")
     //.setValue(0)
     .setPosition(10,890)
     .setSize(200,20)
     ;
 B2 =cp5.addButton("Before")
     //.setValue(0)
     .setPosition(870,890)
     .setSize(200,20)
     ;
   cp5.addLabel("Filtro",350,890);
  l1= cp5.addLabel("0000000",400,890).setText("hola");
 /***************************Camera***********************************************/
  String[] cameras = Capture.list();
  for (int i =0 ; i< cameras.length;i++)
  {
    println("i: "+ i+ " : name= " +  cameras[i]);
  }
  c = new Capture(this, 320, 240, "HP HD Camera", 30);
  c.start();
  if(c.available()){
    c.read();
    img.pixels =c.pixels;
  }
}
boolean flag=true;
void draw()
{
  background(0);
  noStroke();
  if(flag)
  {
   flag= false;
    c.read();
    img.pixels =c.pixels;
  }
  if(c.available()){
   c.read();
   c.loadPixels();
  

   for(int i =0; i < c.width;i++)
 {
   for(int j = 0; j < c.height ; j++)
   {
     int k= i + (j*c.width); /*esto es para rellenar el arreglo de pixels con una matriz*/
     switch(state){
           case 1:
           FiltroGrises(k);
           break;
           case 2:
           BalckandWhite(k);
           break;
           case 3:
           Negativo(k);
           break;
           case 4:
           Diferencia(k);
           break;
        }
     }
  }
   
  c.updatePixels();
 
 
  }
  image(c,0,0);
  
}

public void Next(int valor)
{
  println("boton next presionado:"+valor);
    println(state);
  if(4 == state)
  {
    state=0;
    l1.setText("original");

  }
  else
  {
  state++;
  }
}

public void Before(int valor)
{
  println("boton before presionado:"+valor);
  println(state);
  if(0 == state)
  {
    state=4;
    l1.setText("original");

  }
  else
  {
  state--;
  }
}

void FiltroGrises(int k)
{
  l1.setText("Grises");
        float vActual   = green (c.pixels[k]) ,
                rActual  = red  (c.pixels[k]) ,
                aACtual  = blue (c.pixels[k]) ,
                promedio = ((rActual+vActual+aACtual) / 3);
            
              c.pixels[k]= color(promedio,promedio,promedio);
}
void BalckandWhite(int k)
{
  l1.setText("Blanco y negro");
  float vActual   = green (c.pixels[k]) ,
                rActual  = red  (c.pixels[k]) ,
                aACtual  = blue (c.pixels[k]) ,
                promedio = ((rActual+vActual+aACtual) / 3);
            
              
              if(promedio > limite)
              {
                c.pixels[k]= color(0,0,0);
              }
              else
              {
                 c.pixels[k]= color(255,255,255);
              }
}
void Negativo(int k)
{
  l1.setText("Negativo");
  float vActual  = green (c.pixels[k]) ,
        rActual  = red  (c.pixels[k]) ,
        aACtual  = blue (c.pixels[k]) ;
  c.pixels[k]= color ((255-rActual),(255-vActual),(255-aACtual));
}

void Diferencia(int k)
{
    l1.setText("Diferencias");
  float vActual   = green (c.pixels[k]) ,
                rActual  = red  (c.pixels[k]) ,
                aACtual  = blue (c.pixels[k]) ,
                promedio = ((rActual+vActual+aACtual) / 3);
 float vActual2   = green (img.pixels[k]) ,
                rActual2  = red  (img.pixels[k]) ,
                aACtual2  = blue (img.pixels[k]) ,
                promedio2 = ((rActual2+vActual2+aACtual2) / 3);
  if( limite > (abs(promedio-promedio2)))
  {
    c.pixels[k] = color(0,0,0);
  }
}
void Porcentaje(float r)
{
 println("Porcentaje: " + r);
 limite=int(r);
}